from django.shortcuts import render
from rest_framework import viewsets

from spaces.models import Space, Employee, Reserve
from .serializers import SpaceSerializer, EmployeeSerializer, ReserveSerializer


class SpaceViewSet(viewsets.ModelViewSet):
    serializer_class = SpaceSerializer
    queryset = Space.objects.all()
    

class EmployeeViewSet(viewsets.ModelViewSet):
    serializer_class = EmployeeSerializer
    queryset = Employee.objects.all()
    

class ReserveViewSet(viewsets.ModelViewSet):
    serializer_class = ReserveSerializer
    queryset = Reserve.objects.all()
    
