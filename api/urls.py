from django.urls import path
from rest_framework.routers import DefaultRouter

from .views import SpaceViewSet, EmployeeViewSet, ReserveViewSet


# app_name = 'api'

router = DefaultRouter()

router.register(r'spaces', SpaceViewSet)
router.register(r'employees', EmployeeViewSet)
router.register(r'reserves', ReserveViewSet)

urlpatterns = router.urls
