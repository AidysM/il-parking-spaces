from django.urls import path, include
from django.contrib.auth.views import LoginView, LogoutView

from .views import SpaceList, SpaceDetail, SpaceCreate, SpaceUpdate, SpaceDelete
from .views import ReserveList, ReserveDetail, ReserveCreate, ReserveUpdate, ReserveDelete


app_name = 'spaces'

urlpatterns = [
    path('', SpaceList.as_view(), name='spaces'),
    path('<int:pk>/', SpaceDetail.as_view(), name='space'),
    path('create/', SpaceCreate.as_view(), name='space_create'),
    path('<int:pk>/update/', SpaceUpdate.as_view(), name='space_update'),
    path('<int:pk>/delete/', SpaceDelete.as_view(), name='space_delete'),
    path('login/', LoginView.as_view(template_name='spaces/login.html'), name='login'),
    path('logout/', LogoutView.as_view(template_name = 'spaces/logout.html'), name='logout'),
    path('reserve/', ReserveList.as_view(), name='reserve_list'),
    path('reserve/<int:pk>/', ReserveDetail.as_view(), name='reserve_detail'),
    path('reserve/create/', ReserveCreate.as_view(), name='reserve_create'),
    path('reserve/<int:pk>/update/', ReserveUpdate.as_view(), name='reserve_update'),
    path('reserve/<int:pk>/delete/', ReserveDelete.as_view(), name='reserve_delete'),
]
