from django.shortcuts import render
from django.views import generic
from django.contrib.auth.mixins import PermissionRequiredMixin

from .models import Space, Reserve
from .forms import SpaceForm, ReserveForm


class SpaceList(generic.ListView):
    model = Space
    template_name = 'spaces/list.html'
    context_object_name = 'spaces'
    queryset = Space.objects.all()
    
    
class SpaceDetail(generic.DetailView):
    model = Space
    template_name = 'spaces/detail.html'
    form_class = SpaceForm
    # context_object_name = 'space'
    # queryset = Space.objects.all()


class SpaceCreate(PermissionRequiredMixin, generic.CreateView):
    permission_required = ('spaces.add_space',)
    template_name = 'spaces/space_create.html'
    form_class = SpaceForm
    success_url = '/'
    

class SpaceUpdate(PermissionRequiredMixin, generic.UpdateView):
    permission_required = ('spaces.change_space',)
    template_name = 'spaces/space_create.html'
    form_class = SpaceForm
    success_url = '/'
    
    def get_object(self, **kwargs):
        id = self.kwargs.get('pk')
        return Space.objects.get(pk=id)
    

class SpaceDelete(PermissionRequiredMixin, generic.DeleteView):
    permission_required = ('spaces.delete_space',)
    template_name = 'spaces/space_delete.html'
    queryset = Space.objects.all()
    success_url = '/'
    

# Reserve CRUD
class ReserveList(generic.ListView):
    model = Reserve
    template_name = 'spaces/reserve_list.html'
    context_object_name = 'reserves'
    queryset = Reserve.objects.all()
    

class ReserveDetail(generic.DetailView):
    model = Reserve
    template_name = 'spaces/reserve_detail.html'
    form_class = ReserveForm
    context_object_name = 'reserve'
    

class ReserveCreate(PermissionRequiredMixin, generic.CreateView):
    permission_required = ('spaces.add_reserve',)
    template_name = 'spaces/reserve_create.html'
    form_class = ReserveForm
    success_url = '/reserve/'
    

class ReserveUpdate(PermissionRequiredMixin, generic.CreateView):
    permission_required = ('spaces.change_reserve',)
    template_name = 'spaces/reserve_create.html'
    form_class = ReserveForm
    success_url = '/reserve/'
    
    def get_object(self, **kwargs):
        id = self.kwargs.get('pk')
        return Reserve.objects.get(pk=id)
    

class ReserveDelete(PermissionRequiredMixin, generic.DeleteView):
    permission_required = ('spaces.delete_reserve',)
    template_name = 'spaces/reserve_delete.html'
    queryset = Reserve.objects.all()
    success_url = '/reserve/'
    