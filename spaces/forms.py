from django.forms import ModelForm

from .models import Space, Reserve


class SpaceForm(ModelForm):
    class Meta:
        model = Space
        fields = '__all__'
        

class ReserveForm(ModelForm):
    class Meta:
        model = Reserve
        fields = '__all__'

