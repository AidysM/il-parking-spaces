from django.db import models
from django.contrib.auth.models import User


class Employee(models.Model):
    ROLE_CHOICES = (
        ('MANAGER', 'manager'),
        ('EMPLOYEE', 'employee')
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    role = models.CharField(max_length=10, choices=ROLE_CHOICES, default='employee')
    
    def __str__(self):
        return f'{self.user.username} {self.role}'


class Space(models.Model):
    STATUS_CHOICES = (
        ('BOOKED', 'booked'),
        ('RESERVED', 'reserved'),
        ('RELEASED', 'released')
    )
    
    title = models.CharField(max_length = 128)
    status = models.CharField(max_length = 64, choices=STATUS_CHOICES, default='released')
    
    def __str__(self):
        return f'{self.title}'
    



class Reserve(models.Model):
    space = models.ForeignKey(Space, on_delete=models.CASCADE)
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    start_time = models.DateTimeField()
    duration = models.IntegerField(default=1)
    
    def __str__(self):
        return f'{self.space} at {self.start_time} by {self.employee.user.username}'
    
    
    