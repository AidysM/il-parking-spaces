django==3.2.9
psycopg2-binary==2.9.2
djangorestframework==3.12.4
coreapi==2.3.3
pyyaml==6.0
gunicorn==20.1.0
django-heroku
